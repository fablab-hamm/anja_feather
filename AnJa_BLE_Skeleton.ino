/* 
Date: 17.04.2020

Proof of concept!

Advertises a BLE device with custrom service and custom characteristics.
The state characteristic (1 byte) is used to control a timer. 
Writing a 1 starts the timer and writing a 0 disables the timer.
It is also possible to read the state or enable change notification.
The timer characteristic is a 4 byte long counter that counts up if state is set to 1.

How to setup Arduino IDE:
https://learn.adafruit.com/bluefruit-nrf52-feather-learning-guide/arduino-bsp-setup
*/

#include <bluefruit.h>

const uint8_t ANJA_SERVICE_UUID[16] = {0x71, 0x35, 0xc0, 0x67, 0x09, 0x08, 0xfa, 0x95, 0x72, 0x48, 0x00, 0x00, 0x0f, 0x5d, 0x8c, 0xeb};
const uint8_t ANJA_STATE_CHR_UUID[16] = {0x71, 0x35, 0xc0, 0x67, 0x09, 0x08, 0xfa, 0x95, 0x72, 0x48, 0x01, 0x00, 0x0f, 0x5d, 0x8c, 0xeb};
const uint8_t ANJA_TIMER_CHR_UUID[16] = {0x71, 0x35, 0xc0, 0x67, 0x09, 0x08, 0xfa, 0x95, 0x72, 0x48, 0x02, 0x00, 0x0f, 0x5d, 0x8c, 0xeb};

/* BLE definitions */
BLEService anja_service = BLEService(ANJA_SERVICE_UUID); 
BLECharacteristic state_chr = BLECharacteristic(ANJA_STATE_CHR_UUID);
BLECharacteristic timer_chr = BLECharacteristic(ANJA_TIMER_CHR_UUID);

BLEDis bledis;    // DIS (Device Information Service) helper class instance
BLEBas blebas;    // BAS (Battery Service) helper class instance

void setup() {
  Serial.begin(115200);
  while ( !Serial ) delay(10);   // for nrf52840 with native usb

  Serial.println("An?Ja! BLE Skeleton");
  Serial.println("-----------------------\n");

  // Initialise the Bluefruit module
  Serial.println("Initialise the Bluefruit nRF52 module");
  Bluefruit.begin();

  // Set the advertised device name (keep it short!)
  Serial.println("Setting Device Name to 'An?Ja! 1234'");
  Bluefruit.setName("An?Ja! 1234");

  // Set the connect/disconnect callback handlers
  Bluefruit.Periph.setConnectCallback(connect_callback);
  Bluefruit.Periph.setDisconnectCallback(disconnect_callback);

  // Configure and Start the Device Information Service
  Serial.println("Configuring the Device Information Service");
  bledis.setManufacturer("FabLab Hamm");
  bledis.setModel("An?Ja! Prototype");
  bledis.begin();

  // Start the BLE Battery Service and set it to 100%
  Serial.println("Configuring the Battery Service");
  blebas.begin();
  blebas.write(100);

  // Setup the Heart Rate Monitor service using
  // BLEService and BLECharacteristic classes
  Serial.println("Configuring the An?Ja! Custom Service");
  setupAnJaService();

  // Setup the advertising packet(s)
  Serial.println("Setting up the advertising payload(s)");
  startAdv();

  Serial.println("Ready Player One!!!");
  Serial.println("\nAdvertising");
}

void startAdv(void)
{
  // Advertising packet
  Bluefruit.Advertising.addFlags(BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE);
  Bluefruit.Advertising.addTxPower();

  // Include HRM Service UUID
  Bluefruit.Advertising.addService(anja_service);

  // Include Name
  Bluefruit.Advertising.addName();
  
  /* Start Advertising
   * - Enable auto advertising if disconnected
   * - Interval:  fast mode = 20 ms, slow mode = 152.5 ms
   * - Timeout for fast mode is 30 seconds
   * - Start(timeout) with timeout = 0 will advertise forever (until connected)
   * 
   * For recommended advertising interval
   * https://developer.apple.com/library/content/qa/qa1931/_index.html   
   */
  Bluefruit.Advertising.restartOnDisconnect(true);
  Bluefruit.Advertising.setInterval(32, 244);    // in unit of 0.625 ms
  Bluefruit.Advertising.setFastTimeout(30);      // number of seconds in fast mode
  Bluefruit.Advertising.start(0);                // 0 = Don't stop advertising after n seconds  
}

void setupAnJaService(void)
{

  anja_service.begin();

  state_chr.setProperties(CHR_PROPS_NOTIFY | CHR_PROPS_READ | CHR_PROPS_WRITE);
  state_chr.setPermission(SECMODE_OPEN, SECMODE_OPEN);
  state_chr.setFixedLen(1);
  state_chr.setCccdWriteCallback(cccd_callback);  // Optionally capture CCCD updates
  state_chr.begin();
  state_chr.write8(0);

  timer_chr.setProperties(CHR_PROPS_READ);
  timer_chr.setPermission(SECMODE_OPEN, SECMODE_NO_ACCESS);
  timer_chr.setFixedLen(4);
  timer_chr.begin();
  timer_chr.write32(0); 
}

void connect_callback(uint16_t conn_handle)
{
  // Get the reference to current connection
  BLEConnection* connection = Bluefruit.Connection(conn_handle);

  char central_name[32] = { 0 };
  connection->getPeerName(central_name, sizeof(central_name));

  Serial.print("Connected to ");
  Serial.println(central_name);
}

/**
 * Callback invoked when a connection is dropped
 * @param conn_handle connection where this event happens
 * @param reason is a BLE_HCI_STATUS_CODE which can be found in ble_hci.h
 */
void disconnect_callback(uint16_t conn_handle, uint8_t reason)
{
  (void) conn_handle;
  (void) reason;

  Serial.print("Disconnected, reason = 0x"); Serial.println(reason, HEX);
  Serial.println("Advertising!");
}


void cccd_callback(uint16_t conn_hdl, BLECharacteristic* chr, uint16_t cccd_value)
{
    // Display the raw request packet
    Serial.print("CCCD Updated: ");
    //Serial.printBuffer(request->data, request->len);
    Serial.print(cccd_value);
    Serial.println("");

    // Check the characteristic this CCCD update is associated with in case
    // this handler is used for multiple CCCD records.
    if (chr->uuid == state_chr.uuid) {
        if (chr->notifyEnabled(conn_hdl)) {
            Serial.println("State 'Notify' enabled");
        } else {
            Serial.println("State 'Notify' disabled");
        }
    }
}

uint32_t tmem = 0;

void loop() {
  // put your main code here, to run repeatedly:

  if(state_chr.read8() == 1) {
    timer_chr.write32(tmem++); 
    digitalWrite(LED_RED, HIGH);
  } else {
    digitalToggle(LED_RED);
  }

  delay(1000);
}
